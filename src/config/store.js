import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        isMenuVisible: true,
        user: {
            id:'',
            token:'',
            name:'',
            email:'',
            isAdmin: false
        }
    },
    mutations: {
        toggleMenu(state, isVisible) {
            if(!state.user) {
                state.isMenuVisible = false
                return
            }

            if(isVisible === undefined) {
                state.isMenuVisible = !state.isMenuVisible
            } else {
                state.isMenuVisible = isVisible
            }
        },
        setUser(state, user) {
            if(user) {
                this.state.user = {
                    token: user.token,
                    name: user.userName,
                    id: user.id,
                    isAdmin: user.isAdmin
                }
                axios.defaults.headers.common['Authorization'] = `Bearer ${user.token}`
                state.isMenuVisible = true
            } else {
                delete axios.defaults.headers.common['Authorization']
                this.state.user = user
                state.isMenuVisible = false
            }
        }
        
    }
})